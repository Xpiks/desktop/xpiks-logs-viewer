function strStartsWith(str, prefix) {
    return str.indexOf(prefix) === 0;
}

function loadScript(url, callback) {
    let script = document.createElement("script");
    script.type = 'text/javascript';
    script.src = url;
    script.onload = callback;
    document.head.appendChild(script);
}

function rot47(x) {
    let s=[];
    for(var i=0;i<x.length;i++) {
        let j=x.charCodeAt(i);
        if ((j>=33)&&(j<=126)) {
            s[i]=String.fromCharCode(33+((j+ 14)%94));
        } else {
            s[i]=String.fromCharCode(j);
        }
    }
    return s.join('');
}

function shuffle(array) {
    let currentIndex = array.length, temporaryValue, randomIndex;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

function appendRepeatItem(parent, repeatCount, repeatType) {
    let repeatLineP = document.createElement('p');
    repeatLineP.setAttribute('class', repeatType + ' logitem');
    let repeatLineText = document.createTextNode('--- Same logline was repeated ' + repeatCount + ' more time(s)');
    repeatLineP.appendChild(repeatLineText);
    parent.appendChild(repeatLineP);
}

function parseXpiksLogs(parent, text, decode) {
    let lines = text.split("\n");
    let threads_colors = ['#00897b', '#d32f2f', '#ad1457', '#7b1fa2', '#5e35b1', '#3f51b5', '#039be5', '#0097a7', '#388e3c', '#afb42b', '#fbc02d', '#8d6e63', '#f48fb1', '#4a148c', '#880e4f', '#b71c1c', '#0d47a1', '#004d40', '#006064'];
    shuffle(threads_colors);

    let threads = [{id: "", msg: "All threads"}];

    let usedThreads = {};
    let lastUsedThreads = 0;
    let lastLogLine = '';
    let sameLogLinesCount = 0;
    let lastLogType = '';

    for (let i = 0; i < lines.length; i++) {
        let line = lines[i];
        if (decode) { line = rot47(line); }
        let parts = line.split(' ');

        if ((parts.length < 3) || (!strStartsWith(parts[2], 'T#'))) {
            let rawLine = document.createTextNode(line);
            parent.appendChild(rawLine);
            continue;
        }

        let logLine = parts.slice(3).join(' ');
        if (logLine != lastLogLine) {
            if (sameLogLinesCount > 0) {
                appendRepeatItem(parent, sameLogLinesCount, lastLogType);
            }

            lastLogLine = logLine;
            lastLogType = parts[1];
            sameLogLinesCount = 0;
        } else {
            sameLogLinesCount++;
            continue;
        }

        let element = document.createElement('p');
        element.setAttribute('class', parts[1] + ' logitem');

        let time = document.createTextNode(parts[0] + ' ');
        element.appendChild(time);

        let threadIDElement = document.createElement('span');
        let threadColor = '';
        let threadID = parts[2];
        if (usedThreads.hasOwnProperty(threadID)) {
            threadColor = usedThreads[threadID];
        } else {
            threadColor = threads_colors[lastUsedThreads];
            usedThreads[threadID] = threadColor;
            lastUsedThreads = (lastUsedThreads + 1) % threads_colors.length;
            threads.push({id: threadID, msg: logLine});
        }

        threadIDElement.setAttribute('style', 'color: ' + threadColor);
        threadIDElement.innerHTML = threadID;
        element.appendChild(threadIDElement);

        let logTextElement = document.createElement('span');

        logTextElement.innerHTML = ' ' + logLine;
        element.appendChild(logTextElement);

        parent.appendChild(element);
    }

    return threads;
}

function clearPreviousEntries(element) {
    while (element.lastChild) {
        element.removeChild(element.lastChild);
    }
}

function prepareAndPublishLogs(text) {
    let textDisplayArea = document.getElementById('textDisplayArea');
    clearPreviousEntries(textDisplayArea);
    let decode = document.getElementById("decryptCheckbox").checked;
    let threads = parseXpiksLogs(textDisplayArea, text, decode);
    setThreads(threads);
    textDisplayArea.style.visibility = "visible";
    $(".brd").css({"visibility" : "visible"});
}

function setThreads(threads) {
    var threadsSelect = document.getElementById('threads');
    threadsSelect.innerHTML = '';
    var threadOpt = null;
    for (let i = 0; i < threads.length; i++) {
        let thread = threads[i];
        threadOpt = document.createElement("option");
        threadOpt.value = thread.id;
        threadOpt.innerHTML = "(" + thread.id + ") " + thread.msg;
        threadsSelect.append(threadOpt);
    }
}

function readAndPublish(file) {
    let label = document.getElementById('inputlabel');

    let reader = new FileReader();
    reader.onload = function(e) {
        let text = reader.result;
        prepareAndPublishLogs(text);
        label.querySelector('span').innerHTML = 'File: ' + file.name;
    };

    reader.readAsText(file);
}

function handleDragDrop(event) {
    event.stopPropagation();
    event.preventDefault();

    let files = event.dataTransfer.files;

    if (files && files.length > 0) {
        let f = event.dataTransfer.files[0];
        readAndPublish(f);
    } else {
        let text = event.dataTransfer.getData('text');
        prepareAndPublishLogs(text);
    }

    $('.upload-cont').css('border', '');
}

function openFileDialogHandler(event) {
    if (event.target.files &&
        event.target.files.length) {
        let f = event.target.files[0];
        readAndPublish(f);
    }
}

function handleDragOver(evt) {
    evt.stopPropagation();
    evt.preventDefault();
    evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
    $(".upload-cont").css({"border":"thick dashed white"});
}

function filterLogs() {
    let filterText = document.getElementById('filterText').value.trim();

    let logs = document.querySelectorAll(".logitem");
    for (let i = 0; i < logs.length; i++) {
        let logItem = logs[i];
        if (filterText == '' || logItem.innerHTML.includes(filterText)) {
            logItem.classList.remove("filteredout");
        } else {
            logItem.classList.add("filteredout");
        }
    }

    $('#filteredOutStyle').prop('disabled', false);
}

function resetFiltering() {
    document.getElementById('filterText').value = '';
    $('#filteredOutStyle').prop('disabled', true);
}

function onThreadSelected(selectObj) {
    document.getElementById('filterText').value = selectObj.value;
    filterLogs();
}

function loadLogFromJsFile() {
    if (typeof logsVariable !== "undefined") {
        let text = logsVariable;
        prepareAndPublishLogs(text);

        $(".debug, .info").css({
            "visibility" : "hidden",
            "display" : "none"
        });
    }
}

function toggleWarnings() {
    let onlyWarnings = document.getElementById("togglewarn").checked;
    $('#infoStyle').prop('disabled', onlyWarnings);
    $('#debugStyle').prop('disabled', onlyWarnings);
}

window.onload = function() {
    loadScript('current-log.js', loadLogFromJsFile);

    let fileInput = document.getElementById('files');
    let dropZone = document.getElementById('box');

    fileInput.addEventListener('change', openFileDialogHandler);
    dropZone.addEventListener('dragover', handleDragOver);
    dropZone.addEventListener('drop', handleDragDrop);

    let filterText = document.getElementById('filterText');

    $(filterText).on("paste", function(e) {
        e.preventDefault();
        e.stopPropagation();
        let text = e.originalEvent.clipboardData.getData('text');
        filterText.value = text;
        filterLogs();
    });

    $(document).on("paste", function (e) {
        e.preventDefault();
        e.stopPropagation();
        // Short pause to wait for paste to complete
        let text = e.originalEvent.clipboardData.getData('text');
        prepareAndPublishLogs(text);
    });
};
